import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pendings: any[] = []
  newPending: string = ''
  edit: boolean = false
  private edit_data = {
    id: null,
    pending: {
      name: null
    }
  }

  constructor(
    public navCtrl: NavController, 
    public userService: UserServiceProvider 
  ) {}

  ionViewDidLoad(){
    this.getData()
  }

  addPending(){
    if( this.newPending.length > 0 ){
      this.newData(this.newPending)
    }
  }

  deletePending(param: string){
    if(param.length > 0){
      this.deleteData(param)
    }
  }

  editPending(param: string, name: string, option: number){
    switch (option) {
      case 1:
          this.edit_data.id = param
          this.newPending = name
          this.edit = true
        break;
      case 2:
        if( this.newPending.length > 0){
          this.edit_data.pending.name = this.newPending
          this.newPending = ''
          this.edtiData() 
        }
        break;
      default:
        console.error('Unkown error')
        break;
    }
  }

  private edtiData(){
    this.userService.putPending(this.edit_data.id, this.edit_data.pending).subscribe(
      (response: any) => {
        //console.log(response)
        this.getData()
        this.edit = false
      },
      err => { console.error(err); this.edit = false }
    )
  }

  private deleteData(_id : string){
    this.userService.deletePending(_id).subscribe(
      (response: any) => {
       //console.log(response)
        this.getData()
      },
      err => console.error(err)
    )
  }

  private newData(value : string) {
    const data = {
      name: value
    }
    this.userService.postPending(data).subscribe(
      (response: any) => {
        //console.log(response)
        this.newPending = ''
        this.getData()
      },
      err => console.error(err)
    )
  }

  private getData(){
     this.userService.getPending().subscribe(
      (data: any) => {
        this.pendings = data.pendings
        //console.log(this.pendings)
      },
      err => {
        err => console.error(err)
      }
    )
  }


}
