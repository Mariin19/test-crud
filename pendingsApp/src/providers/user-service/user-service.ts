import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserServiceProvider {

  private urlweb: string = 'http://localhost:3700/api'

  constructor(public http: HttpClient) {}

  getPending(){
    return this.http.get(`${this.urlweb}/home`);
  }

  postPending(data){
    return this.http.post(`${this.urlweb}/save/pending`, data);
  }

  deletePending(id){
    return this.http.delete(`${this.urlweb}/delete/pending/` + id)
  }

  putPending(id, data){
    return this.http.put(`${this.urlweb}/update/pending/` + id, data)
  }

}
