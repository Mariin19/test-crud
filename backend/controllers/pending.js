'use strict'

var Pending = require('../models/pending');

var controller = {

    getPendings: function(req, res) {
        Pending.find({}, (err, pendings) => {
            if (err) return res.status(500).send({ message: "Error al devolver datos" });
            if (!pendings) return res.status(404).send({ message: "No hay pendientes" });
            return res.status(200).send({
                pendings: pendings
            });
        })
    },

    postPending: function(req, res) {
        var pending = new Pending();
        var params = req.body;
        pending.name = params.name;
        pending.save((err, pendingStored) => {
            if (err) return res.status(500).send({ message: "error al guardar" })

            if (!pendingStored) return res.status(404).send({ message: "No se ha podido guardar el pendiente" })

            return res.status(200).send({ pending: pendingStored, message: 'Pediente Guardado' });
        })
    },

    putPending: function(req, res) {
        var pendingId = req.params.id;
        var update = req.body;

        Pending.findByIdAndUpdate(pendingId, update, {new: true}, (err, pendingUpdate) => {
            if (err) return res.status(500).send({ message: "Error al actualizar" });
            if (!pendingUpdate) return res.status(404).send({ message: "No se ha podido actualizar" });
            return res.status(200).send({
                pending: pendingUpdate,
                message: 'Pendiente Actualizado'
            })
        })
    },

    deletePending: function(req, res) {
        var pendingId =  req.params.id;
        Pending.findByIdAndRemove(pendingId, (err, pendingRemoved) => {
            if (err) return res.status(500).send({ message: "No se ha podido borrar el pendiente" });
            if (!pendingRemoved) return res.status(404).send({ message: "No se puede eliminar ese pendiente" });
            return res.status(200).send({
                pending: pendingRemoved,
                message: 'Pendiente Eliminado'
            })
        })
    }

}

module.exports = controller