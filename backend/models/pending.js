'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PendingSchema = Schema({
    name: String
});

module.exports = mongoose.model('Pending', PendingSchema);