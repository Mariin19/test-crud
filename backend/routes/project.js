'use strict'

var express = require('express');
var PendingController = require('../controllers/pending');

var router = express.Router();


router.get('/home', PendingController.getPendings);
router.post('/save/pending', PendingController.postPending);
router.put('/update/pending/:id', PendingController.putPending );
router.delete('/delete/pending/:id', PendingController.deletePending)
module.exports = router;